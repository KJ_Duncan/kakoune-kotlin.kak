# References:
#
# Kotlin 2021, All Types, Kotlin Standard Library, v1.5.20, viewed 01 July 2021, <https://kotlinlang.org/api/latest/jvm/stdlib/alltypes/>
# Kotlin 2021, All Types, kotlinx Coroutines Core, viewed 01 July 2021, <https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/alltypes/index.html>
# Kotlin 2021, All Types, Kotlin Test, viewed 01 July 2021, <https://kotlinlang.org/api/latest/kotlin.test/alltypes/>
#
hook global WinSetOption filetype=kotlin %{
  require-module kotlin

  declare-option -hidden completions kotlin_hierarchy_completions

  set-option window completers option=kotlin_hierarchy_completions %opt{completers}

  hook -group kotlin-import-statements window InsertCompletionShow .* %{
    try %{
      execute-keys -draft 2b s \A^import<space>\z<ret>

      evaluate-commands -draft %{
        execute-keys h <a-i>w <a-semicolon>

        set-option window kotlin_hierarchy_completions \
          "%val{cursor_line}.%val{cursor_column}+%val{selection_length}@%val{timestamp}"
      }
      # --------------- STAGE 1 OF 4 --------------------------------------------------------------- #
      evaluate-commands %sh{

        javaClassHierarchy='java.io.BufferedInputStream java.io.BufferedReader java.io.File
          java.io.InputStream java.io.OutputStream java.io.Reader java.io.Writer java.io.Writer
          java.lang.Appendable java.lang.Class java.lang.StringBuilder java.lang.ThreadLocal
          java.lang.reflect.Constructor java.lang.reflect.Field java.lang.reflect.Method java.math.BigDecimal
          java.math.BigInteger java.net.URI java.net.URL java.nio.file.Path java.time.Duration
          java.util.Enumeration java.util.Enumeration java.util.Random java.util.Timer
          java.util.concurrent.ConcurrentMap java.util.concurrent.Executor java.util.concurrent.ExecutorService
          java.util.concurrent.locks.Lock java.util.concurrent.locks.ReentrantReadWriteLock
          java.util.regex.Pattern java.util.stream.DoubleStream java.util.stream.IntStream
          java.util.stream.LongStream java.util.stream.Stream'


        kotlinClassHierarchy='kotlin.Annotation kotlin.Annotation kotlin.Any kotlin.Any
          kotlin.ArithmeticException kotlin.Array kotlin.ArrayIndexOutOfBoundsException kotlin.AssertionError
          kotlin.Boolean kotlin.BooleanArray kotlin.BuilderInference kotlin.Byte kotlin.ByteArray kotlin.Char
          kotlin.CharArray kotlin.CharSequence kotlin.ClassCastException kotlin.Comparable kotlin.Comparator
          kotlin.ConcurrentModificationException kotlin.DeepRecursiveFunction kotlin.DeepRecursiveScope
          kotlin.Deprecated kotlin.DeprecatedSinceKotlin kotlin.DeprecationLevel kotlin.Double
          kotlin.DoubleArray kotlin.DslMarker kotlin.Enum kotlin.Error kotlin.Exception kotlin.Experimental
          kotlin.ExperimentalMultiplatform kotlin.ExperimentalStdlibApi kotlin.ExperimentalUnsignedTypes
          kotlin.ExtensionFunctionType kotlin.Float kotlin.FloatArray kotlin.Function kotlin.Function0
          kotlin.IllegalArgumentException kotlin.IllegalStateException kotlin.IndexOutOfBoundsException
          kotlin.Int kotlin.IntArray kotlin.KotlinNullPointerException kotlin.KotlinVersion kotlin.Lazy
          kotlin.LazyThreadSafetyMode kotlin.Long kotlin.LongArray kotlin.Metadata
          kotlin.NoSuchElementException kotlin.NoWhenBranchMatchedException kotlin.NotImplementedError
          kotlin.Nothing kotlin.NullPointerException kotlin.Number kotlin.NumberFormatException kotlin.OptIn
          kotlin.OptionalExpectation kotlin.OutOfMemoryError kotlin.OverloadResolutionByLambdaReturnType
          kotlin.Pair kotlin.ParameterName kotlin.PublishedApi kotlin.ReplaceWith kotlin.RequiresOptIn
          kotlin.Result kotlin.RuntimeException kotlin.Short kotlin.ShortArray kotlin.SinceKotlin kotlin.String
          kotlin.Suppress kotlin.Throwable kotlin.Throws kotlin.Triple kotlin.TypeCastException kotlin.UByte
          kotlin.UByteArray kotlin.UInt kotlin.UIntArray kotlin.ULong kotlin.ULongArray kotlin.UShort
          kotlin.UShortArray kotlin.UninitializedPropertyAccessException kotlin.Unit kotlin.UnsafeVariance
          kotlin.UnsupportedOperationException kotlin.UseExperimental'

        kotlinAnnotation='kotlin.annotation.AnnotationRetention kotlin.annotation.AnnotationRetention
          kotlin.annotation.AnnotationTarget kotlin.annotation.AnnotationTarget
          kotlin.annotation.MustBeDocumented kotlin.annotation.Repeatable kotlin.annotation.Retention
          kotlin.annotation.Target'

        kotlinCollections='kotlin.collections.AbstractCollection kotlin.collections.AbstractCollection
          kotlin.collections.AbstractIterator kotlin.collections.AbstractIterator
          kotlin.collections.AbstractList kotlin.collections.AbstractList kotlin.collections.AbstractMap
          kotlin.collections.AbstractMap kotlin.collections.AbstractMutableCollection
          kotlin.collections.AbstractMutableCollection kotlin.collections.AbstractMutableList
          kotlin.collections.AbstractMutableList kotlin.collections.AbstractMutableMap
          kotlin.collections.AbstractMutableMap kotlin.collections.AbstractMutableSet
          kotlin.collections.AbstractMutableSet kotlin.collections.AbstractSet kotlin.collections.AbstractSet
          kotlin.collections.ArrayDeque kotlin.collections.ArrayList kotlin.collections.BooleanIterator
          kotlin.collections.ByteIterator kotlin.collections.CharIterator kotlin.collections.Collection
          kotlin.collections.DoubleIterator kotlin.collections.FloatIterator kotlin.collections.Grouping
          kotlin.collections.HashMap kotlin.collections.HashSet kotlin.collections.IndexedValue
          kotlin.collections.IntIterator kotlin.collections.Iterable kotlin.collections.Iterator
          kotlin.collections.LinkedHashMap kotlin.collections.LinkedHashSet kotlin.collections.List
          kotlin.collections.ListIterator kotlin.collections.LongIterator kotlin.collections.Map
          kotlin.collections.MutableCollection kotlin.collections.MutableIterable
          kotlin.collections.MutableIterator kotlin.collections.MutableList
          kotlin.collections.MutableListIterator kotlin.collections.MutableMap kotlin.collections.MutableSet
          kotlin.collections.RandomAccess kotlin.collections.Set kotlin.collections.ShortIterator
          kotlin.collections.UByteIterator kotlin.collections.UIntIterator kotlin.collections.ULongIterator
          kotlin.collections.UShortIterator'

        kotlinContracts='kotlin.contracts.CallsInPlace kotlin.contracts.ConditionalEffect
          kotlin.contracts.ContractBuilder kotlin.contracts.Effect kotlin.contracts.ExperimentalContracts
          kotlin.contracts.InvocationKind kotlin.contracts.Returns kotlin.contracts.ReturnsNotNull
          kotlin.contracts.SimpleEffect'

        kotlinCoroutines='kotlin.coroutines.AbstractCoroutineContextElement
          kotlin.coroutines.AbstractCoroutineContextElement kotlin.coroutines.AbstractCoroutineContextKey
          kotlin.coroutines.AbstractCoroutineContextKey kotlin.coroutines.Continuation
          kotlin.coroutines.ContinuationInterceptor kotlin.coroutines.CoroutineContext
          kotlin.coroutines.EmptyCoroutineContext kotlin.coroutines.RestrictsSuspension
          kotlin.coroutines.SuspendFunction kotlin.coroutines.cancellation.CancellationException'

        kotlinExperimental='kotlin.experimental.ExperimentalTypeInference'

        kotlinIO='kotlin.io.AccessDeniedException kotlin.io.AccessDeniedException
          kotlin.io.FileAlreadyExistsException kotlin.io.FileSystemException kotlin.io.FileTreeWalk
          kotlin.io.FileWalkDirection kotlin.io.NoSuchFileException kotlin.io.OnErrorAction
          kotlin.io.path.ExperimentalPathApi'

        kotlinJs='kotlin.js.Console kotlin.js.Date kotlin.js.ExperimentalJsExport kotlin.js.JSON
          kotlin.js.JsClass kotlin.js.JsExport kotlin.js.JsModule kotlin.js.JsName kotlin.js.JsNonModule
          kotlin.js.JsQualifier kotlin.js.Json kotlin.js.Promise kotlin.js.RegExp kotlin.js.RegExpMatch
          kotlin.js.nativeGetter kotlin.js.nativeInvoke kotlin.js.nativeSetter kotlin.js.Promise'

        kotlinJvm='kotlin.jvm.JvmDefault kotlin.jvm.JvmDefaultWithoutCompatibility kotlin.jvm.JvmField
          kotlin.jvm.JvmInline kotlin.jvm.JvmMultifileClass kotlin.jvm.JvmName kotlin.jvm.JvmOverloads
          kotlin.jvm.JvmRecord kotlin.jvm.JvmStatic kotlin.jvm.JvmSuppressWildcards kotlin.jvm.JvmSynthetic
          kotlin.jvm.JvmWildcard kotlin.jvm.KotlinReflectionNotSupportedError kotlin.jvm.PurelyImplements
          kotlin.jvm.Strictfp kotlin.jvm.Synchronized kotlin.jvm.Throws kotlin.jvm.Transient
          kotlin.jvm.Volatile'

        kotlinNative='kotlin.native.BitSet kotlin.native.CName kotlin.native.CpuArchitecture
          kotlin.native.ImmutableBlob kotlin.native.IncorrectDereferenceException kotlin.native.MemoryModel
          kotlin.native.OsFamily kotlin.native.Platform kotlin.native.ReportUnhandledExceptionHook
          kotlin.native.Retain kotlin.native.RetainForTarget kotlin.native.SymbolName kotlin.native.Vector128
          kotlin.native.concurrent.AtomicInt kotlin.native.concurrent.AtomicLong
          kotlin.native.concurrent.AtomicNativePtr kotlin.native.concurrent.AtomicReference
          kotlin.native.concurrent.Continuation0 kotlin.native.concurrent.Continuation1
          kotlin.native.concurrent.Continuation2 kotlin.native.concurrent.DetachedObjectGraph
          kotlin.native.concurrent.FreezableAtomicReference kotlin.native.concurrent.FreezingException
          kotlin.native.concurrent.Future kotlin.native.concurrent.FutureState
          kotlin.native.concurrent.InvalidMutabilityException kotlin.native.concurrent.MutableData
          kotlin.native.concurrent.SharedImmutable kotlin.native.concurrent.ThreadLocal
          kotlin.native.concurrent.TransferMode kotlin.native.concurrent.Worker
          kotlin.native.concurrent.WorkerBoundReference kotlin.native.ref.WeakReference'

        kotlinProperties='kotlin.properties.Delegates kotlin.properties.ObservableProperty
          kotlin.properties.PropertyDelegateProvider kotlin.properties.ReadOnlyProperty
          kotlin.properties.ReadWriteProperty'

        kotlinRandom='kotlin.random.Random'

        kotinRanges='kotlin.ranges.CharProgression kotlin.ranges.CharRange
          kotlin.ranges.ClosedFloatingPointRange kotlin.ranges.ClosedRange kotlin.ranges.IntProgression
          kotlin.ranges.IntRange kotlin.ranges.LongProgression kotlin.ranges.LongRange
          kotlin.ranges.UIntProgression kotlin.ranges.UIntRange kotlin.ranges.ULongProgression
          kotlin.ranges.ULongRange'

        kotlinReflect='kotlin.reflect.AssociatedObjectKey kotlin.reflect.ExperimentalAssociatedObjects
          kotlin.reflect.KAnnotatedElement kotlin.reflect.KCallable kotlin.reflect.KClass
          kotlin.reflect.KClassifier kotlin.reflect.KDeclarationContainer kotlin.reflect.KFunction
          kotlin.reflect.KMutableProperty kotlin.reflect.KMutableProperty0 kotlin.reflect.KMutableProperty1
          kotlin.reflect.KMutableProperty2 kotlin.reflect.KParameter kotlin.reflect.KProperty
          kotlin.reflect.KProperty0 kotlin.reflect.KProperty1 kotlin.reflect.KProperty2 kotlin.reflect.KType
          kotlin.reflect.KTypeParameter kotlin.reflect.KTypeProjection kotlin.reflect.KVariance
          kotlin.reflect.KVisibility kotlin.reflect.full.IllegalCallableAccessException
          kotlin.reflect.full.IllegalPropertyDelegateAccessException
          kotlin.reflect.full.NoSuchPropertyException kotlin.reflect.jvm.ExperimentalReflectionOnLambdas'

        kotlinSequences='kotlin.sequences.Sequence kotlin.sequences.Sequence kotlin.sequences.SequenceBuilder
          kotlin.sequences.SequenceScope'

        kotlinText='kotlin.text.Appendable kotlin.text.Appendable kotlin.text.CharCategory
          kotlin.text.CharDirectionality kotlin.text.CharacterCodingException kotlin.text.Charsets
          kotlin.text.MatchGroup kotlin.text.MatchGroupCollection kotlin.text.MatchNamedGroupCollection
          kotlin.text.MatchResult kotlin.text.Regex kotlin.text.RegexOption kotlin.text.StringBuilder
          kotlin.text.Typography'

        kotlinTime='kotlin.time.AbstractDoubleTimeSource kotlin.time.AbstractDoubleTimeSource
          kotlin.time.AbstractLongTimeSource kotlin.time.AbstractLongTimeSource kotlin.time.Duration
          kotlin.time.DurationUnit kotlin.time.ExperimentalTime kotlin.time.TestTimeSource kotlin.time.TimeMark
          kotlin.time.TimeSource kotlin.time.TimedValue'

        kotlinxCinterop='kotlinx.cinterop.Arena kotlinx.cinterop.ArenaBase kotlinx.cinterop.AutofreeScope
          kotlinx.cinterop.BooleanVar kotlinx.cinterop.BooleanVarOf kotlinx.cinterop.ByteVar
          kotlinx.cinterop.ByteVarOf kotlinx.cinterop.CArrayPointer kotlinx.cinterop.CArrayPointerVar
          kotlinx.cinterop.CEnum kotlinx.cinterop.CEnumVar kotlinx.cinterop.CFunction kotlinx.cinterop.COpaque
          kotlinx.cinterop.COpaquePointer kotlinx.cinterop.COpaquePointerVar kotlinx.cinterop.CPointed
          kotlinx.cinterop.CPointer kotlinx.cinterop.CPointerVar kotlinx.cinterop.CPointerVarOf
          kotlinx.cinterop.CPrimitiveVar kotlinx.cinterop.CStructVar kotlinx.cinterop.CValue
          kotlinx.cinterop.CValues kotlinx.cinterop.CValuesRef kotlinx.cinterop.CVariable
          kotlinx.cinterop.DeferScope kotlinx.cinterop.DoubleVar kotlinx.cinterop.DoubleVarOf
          kotlinx.cinterop.ExportObjCClass kotlinx.cinterop.ExternalObjCClass kotlinx.cinterop.FloatVar
          kotlinx.cinterop.FloatVarOf kotlinx.cinterop.ForeignException kotlinx.cinterop.IntVar
          kotlinx.cinterop.IntVarOf kotlinx.cinterop.InteropStubs kotlinx.cinterop.LongVar
          kotlinx.cinterop.LongVarOf kotlinx.cinterop.MemScope kotlinx.cinterop.NativeFreeablePlacement
          kotlinx.cinterop.NativePlacement kotlinx.cinterop.NativePointed kotlinx.cinterop.NativePtr
          kotlinx.cinterop.ObjCAction kotlinx.cinterop.ObjCBlockVar kotlinx.cinterop.ObjCClass
          kotlinx.cinterop.ObjCClassOf kotlinx.cinterop.ObjCConstructor kotlinx.cinterop.ObjCFactory
          kotlinx.cinterop.ObjCMethod kotlinx.cinterop.ObjCNotImplementedVar kotlinx.cinterop.ObjCObject
          kotlinx.cinterop.ObjCObjectBase kotlinx.cinterop.ObjCObjectBaseMeta kotlinx.cinterop.ObjCObjectMeta
          kotlinx.cinterop.ObjCObjectVar kotlinx.cinterop.ObjCOutlet kotlinx.cinterop.ObjCProtocol
          kotlinx.cinterop.ObjCStringVarOf kotlinx.cinterop.Pinned kotlinx.cinterop.ShortVar
          kotlinx.cinterop.ShortVarOf kotlinx.cinterop.StableObjPtr kotlinx.cinterop.StableRef
          kotlinx.cinterop.UByteVar kotlinx.cinterop.UByteVarOf kotlinx.cinterop.UIntVar
          kotlinx.cinterop.UIntVarOf kotlinx.cinterop.ULongVar kotlinx.cinterop.ULongVarOf
          kotlinx.cinterop.UShortVar kotlinx.cinterop.UShortVarOf kotlinx.cinterop.Vector128Var
          kotlinx.cinterop.Vector128VarOf kotlinx.cinterop.internal.CCall
          kotlinx.cinterop.internal.CEnumEntryAlias kotlinx.cinterop.internal.CEnumVarTypeSize
          kotlinx.cinterop.internal.CStruct kotlinx.cinterop.internal.ConstantValue
          kotlinx.cinterop.nativeHeap'

        kotlinxWasn='kotlinx.wasm.jsinterop.Arena kotlinx.wasm.jsinterop.ArenaManager
          kotlinx.wasm.jsinterop.JsArray kotlinx.wasm.jsinterop.JsValue kotlinx.wasm.jsinterop.KtFunction
          kotlinx.wasm.jsinterop.Object kotlinx.wasm.jsinterop.Pointer'

        orgKhronos='org.khronos.webgl.ArrayBuffer org.khronos.webgl.ArrayBufferView
          org.khronos.webgl.BufferDataSource org.khronos.webgl.DataView org.khronos.webgl.Float32Array
          org.khronos.webgl.Float64Array org.khronos.webgl.Int16Array org.khronos.webgl.Int32Array
          org.khronos.webgl.Int8Array org.khronos.webgl.TexImageSource org.khronos.webgl.Uint16Array
          org.khronos.webgl.Uint32Array org.khronos.webgl.Uint8Array org.khronos.webgl.Uint8ClampedArray
          org.khronos.webgl.WebGLActiveInfo org.khronos.webgl.WebGLBuffer
          org.khronos.webgl.WebGLContextAttributes org.khronos.webgl.WebGLContextEvent
          org.khronos.webgl.WebGLContextEventInit org.khronos.webgl.WebGLFramebuffer
          org.khronos.webgl.WebGLObject org.khronos.webgl.WebGLProgram org.khronos.webgl.WebGLRenderbuffer
          org.khronos.webgl.WebGLRenderingContext org.khronos.webgl.WebGLRenderingContextBase
          org.khronos.webgl.WebGLShader org.khronos.webgl.WebGLShaderPrecisionFormat
          org.khronos.webgl.WebGLTexture org.khronos.webgl.WebGLUniformLocation'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window kotlin_hierarchy_completions "

        print_completers $javaClassHierarchy $kotlinClassHierarchy $kotlinAnnotation
        print_completers $kotlinAnnotation $kotlinCollections $kotlinContracts
        print_completers $kotlinCoroutines $kotlinExperimental $kotlinIO
        print_completers $kotlinJs $kotlinJvm $kotlinNative $kotlinProperties
        print_completers $kotlinRandom $kotinRanges $kotlinReflect
        print_completers $kotlinSequences $kotlinText $kotlinTime
        print_completers $kotlinxCinterop $kotlinxWasn $orgKhronos

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }

      # --------------- STAGE 2 OF 4 --------------------------------------------------------------- #
      evaluate-commands %sh{

        kotlinxCoroutines='kotlinx.coroutines
          kotlinx.coroutines.flow.AbstractFlow
          kotlinx.coroutines.channels.ActorScope
          kotlinx.coroutines.channels.BroadcastChannel
          kotlinx.coroutines.channels.BufferOverflow
          kotlinx.coroutines.CancellableContinuation
          kotlinx.coroutines.CancellationException
          kotlinx.coroutines.channels.Channel
          kotlinx.coroutines.channels.ChannelIterator
          kotlinx.coroutines.channels.ChannelResult
          kotlinx.coroutines.channels.ClosedReceiveChannelException
          kotlinx.coroutines.channels.ClosedSendChannelException
          kotlinx.coroutines.CompletableDeferred
          kotlinx.coroutines.CompletableJob
          kotlinx.coroutines.CompletionHandler
          kotlinx.coroutines.channels.ConflatedBroadcastChannel
          kotlinx.coroutines.CopyableThrowable
          kotlinx.coroutines.CoroutineDispatcher
          kotlinx.coroutines.CoroutineExceptionHandler
          kotlinx.coroutines.CoroutineName
          kotlinx.coroutines.CoroutineScope
          kotlinx.coroutines.CoroutineStart
          kotlinx.coroutines.Deferred
          kotlinx.coroutines.DelicateCoroutinesApi
          kotlinx.coroutines.Dispatchers
          kotlinx.coroutines.DisposableHandle
          kotlinx.coroutines.ExecutorCoroutineDispatcher
          kotlinx.coroutines.ExperimentalCoroutinesApi
          kotlinx.coroutines.flow.Flow
          kotlinx.coroutines.flow.FlowCollector
          kotlinx.coroutines.FlowPreview
          kotlinx.coroutines.GlobalScope
          kotlinx.coroutines.InternalCoroutinesApi
          kotlinx.coroutines.Job
          kotlinx.coroutines.MainCoroutineDispatcher
          kotlinx.coroutines.flow.MutableSharedFlow
          kotlinx.coroutines.flow.MutableStateFlow
          kotlinx.coroutines.sync.Mutex
          kotlinx.coroutines.NonCancellable
          kotlinx.coroutines.ObsoleteCoroutinesApi
          kotlinx.coroutines.channels.ProducerScope
          kotlinx.coroutines.channels.ReceiveChannel
          kotlinx.coroutines.Runnable
          kotlinx.coroutines.selects.SelectBuilder
          kotlinx.coroutines.selects.SelectClause0
          kotlinx.coroutines.selects.SelectClause1
          kotlinx.coroutines.selects.SelectClause2
          kotlinx.coroutines.sync.Semaphore
          kotlinx.coroutines.channels.SendChannel
          kotlinx.coroutines.flow.SharedFlow
          kotlinx.coroutines.flow.SharingCommand
          kotlinx.coroutines.flow.SharingStarted
          kotlinx.coroutines.flow.StateFlow
          kotlinx.coroutines.test.TestCoroutineContext
          kotlinx.coroutines.ThreadContextElement
          kotlinx.coroutines.channels.TickerMode
          kotlinx.coroutines.TimeoutCancellationException'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window kotlin_hierarchy_completions "

        print_completers $kotlinxCoroutines

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 3 OF 4 --------------------------------------------------------------- #
      evaluate-commands %sh{

        orgW3c='org.w3c.css.masking.SVGClipPathElement org.w3c.css.masking.SVGMaskElement
          org.w3c.dom.AbstractWorker org.w3c.dom.AbstractWorker org.w3c.dom.AddEventListenerOptions
          org.w3c.dom.AddEventListenerOptions org.w3c.dom.ApplicationCache org.w3c.dom.AssignedNodesOptions
          org.w3c.dom.Attr org.w3c.dom.Audio org.w3c.dom.AudioTrack org.w3c.dom.AudioTrackList
          org.w3c.dom.BarProp org.w3c.dom.BeforeUnloadEvent org.w3c.dom.BinaryType org.w3c.dom.BoxQuadOptions
          org.w3c.dom.BroadcastChannel org.w3c.dom.CDATASection org.w3c.dom.CSSBoxType
          org.w3c.dom.CanPlayTypeResult org.w3c.dom.CanvasCompositing org.w3c.dom.CanvasDirection
          org.w3c.dom.CanvasDrawImage org.w3c.dom.CanvasDrawPath org.w3c.dom.CanvasFillRule
          org.w3c.dom.CanvasFillStrokeStyles org.w3c.dom.CanvasFilters org.w3c.dom.CanvasGradient
          org.w3c.dom.CanvasHitRegion org.w3c.dom.CanvasImageData org.w3c.dom.CanvasImageSmoothing
          org.w3c.dom.CanvasImageSource org.w3c.dom.CanvasLineCap org.w3c.dom.CanvasLineJoin
          org.w3c.dom.CanvasPath org.w3c.dom.CanvasPathDrawingStyles org.w3c.dom.CanvasPattern
          org.w3c.dom.CanvasRect org.w3c.dom.CanvasRenderingContext2D
          org.w3c.dom.CanvasRenderingContext2DSettings org.w3c.dom.CanvasShadowStyles org.w3c.dom.CanvasState
          org.w3c.dom.CanvasText org.w3c.dom.CanvasTextAlign org.w3c.dom.CanvasTextBaseline
          org.w3c.dom.CanvasTextDrawingStyles org.w3c.dom.CanvasTransform org.w3c.dom.CanvasUserInterface
          org.w3c.dom.CaretPosition org.w3c.dom.CharacterData org.w3c.dom.ChildNode org.w3c.dom.CloseEvent
          org.w3c.dom.CloseEventInit org.w3c.dom.ColorSpaceConversion org.w3c.dom.Comment
          org.w3c.dom.ConvertCoordinateOptions org.w3c.dom.CustomElementRegistry org.w3c.dom.CustomEvent
          org.w3c.dom.CustomEventInit org.w3c.dom.DOMImplementation org.w3c.dom.DOMMatrix
          org.w3c.dom.DOMMatrixReadOnly org.w3c.dom.DOMPoint org.w3c.dom.DOMPointInit
          org.w3c.dom.DOMPointReadOnly org.w3c.dom.DOMQuad org.w3c.dom.DOMRect org.w3c.dom.DOMRectInit
          org.w3c.dom.DOMRectList org.w3c.dom.DOMRectReadOnly org.w3c.dom.DOMStringMap org.w3c.dom.DOMTokenList
          org.w3c.dom.DataTransfer org.w3c.dom.DataTransferItem org.w3c.dom.DataTransferItemList
          org.w3c.dom.DedicatedWorkerGlobalScope org.w3c.dom.Document
          org.w3c.dom.DocumentAndElementEventHandlers org.w3c.dom.DocumentFragment
          org.w3c.dom.DocumentOrShadowRoot org.w3c.dom.DocumentReadyState org.w3c.dom.DocumentType
          org.w3c.dom.DragEvent org.w3c.dom.DragEventInit org.w3c.dom.Element
          org.w3c.dom.ElementContentEditable org.w3c.dom.ElementCreationOptions
          org.w3c.dom.ElementDefinitionOptions org.w3c.dom.ErrorEvent org.w3c.dom.ErrorEventInit
          org.w3c.dom.EventInit org.w3c.dom.EventListenerOptions org.w3c.dom.EventSource
          org.w3c.dom.EventSourceInit org.w3c.dom.External org.w3c.dom.GeometryUtils
          org.w3c.dom.GetRootNodeOptions org.w3c.dom.GlobalEventHandlers org.w3c.dom.HTMLAllCollection
          org.w3c.dom.HTMLAnchorElement org.w3c.dom.HTMLAppletElement org.w3c.dom.HTMLAreaElement
          org.w3c.dom.HTMLAudioElement org.w3c.dom.HTMLBRElement org.w3c.dom.HTMLBaseElement
          org.w3c.dom.HTMLBodyElement org.w3c.dom.HTMLButtonElement org.w3c.dom.HTMLCanvasElement
          org.w3c.dom.HTMLCollection org.w3c.dom.HTMLDListElement org.w3c.dom.HTMLDataElement
          org.w3c.dom.HTMLDataListElement org.w3c.dom.HTMLDetailsElement org.w3c.dom.HTMLDialogElement
          org.w3c.dom.HTMLDirectoryElement org.w3c.dom.HTMLDivElement org.w3c.dom.HTMLElement
          org.w3c.dom.HTMLEmbedElement org.w3c.dom.HTMLFieldSetElement org.w3c.dom.HTMLFontElement
          org.w3c.dom.HTMLFormControlsCollection org.w3c.dom.HTMLFormElement org.w3c.dom.HTMLFrameElement
          org.w3c.dom.HTMLFrameSetElement org.w3c.dom.HTMLHRElement org.w3c.dom.HTMLHeadElement
          org.w3c.dom.HTMLHeadingElement org.w3c.dom.HTMLHtmlElement org.w3c.dom.HTMLHyperlinkElementUtils
          org.w3c.dom.HTMLIFrameElement org.w3c.dom.HTMLImageElement org.w3c.dom.HTMLInputElement
          org.w3c.dom.HTMLKeygenElement org.w3c.dom.HTMLLIElement org.w3c.dom.HTMLLabelElement
          org.w3c.dom.HTMLLegendElement org.w3c.dom.HTMLLinkElement org.w3c.dom.HTMLMapElement
          org.w3c.dom.HTMLMarqueeElement org.w3c.dom.HTMLMediaElement org.w3c.dom.HTMLMenuElement
          org.w3c.dom.HTMLMenuItemElement org.w3c.dom.HTMLMetaElement org.w3c.dom.HTMLMeterElement
          org.w3c.dom.HTMLModElement org.w3c.dom.HTMLOListElement org.w3c.dom.HTMLObjectElement
          org.w3c.dom.HTMLOptGroupElement org.w3c.dom.HTMLOptionElement org.w3c.dom.HTMLOptionsCollection
          org.w3c.dom.HTMLOrSVGImageElement org.w3c.dom.HTMLOrSVGScriptElement org.w3c.dom.HTMLOutputElement
          org.w3c.dom.HTMLParagraphElement org.w3c.dom.HTMLParamElement org.w3c.dom.HTMLPictureElement
          org.w3c.dom.HTMLPreElement org.w3c.dom.HTMLProgressElement org.w3c.dom.HTMLQuoteElement
          org.w3c.dom.HTMLScriptElement org.w3c.dom.HTMLSelectElement org.w3c.dom.HTMLSlotElement
          org.w3c.dom.HTMLSourceElement org.w3c.dom.HTMLSpanElement org.w3c.dom.HTMLStyleElement
          org.w3c.dom.HTMLTableCaptionElement org.w3c.dom.HTMLTableCellElement org.w3c.dom.HTMLTableColElement
          org.w3c.dom.HTMLTableElement org.w3c.dom.HTMLTableRowElement org.w3c.dom.HTMLTableSectionElement
          org.w3c.dom.HTMLTemplateElement org.w3c.dom.HTMLTextAreaElement org.w3c.dom.HTMLTimeElement
          org.w3c.dom.HTMLTitleElement org.w3c.dom.HTMLTrackElement org.w3c.dom.HTMLUListElement
          org.w3c.dom.HTMLUnknownElement org.w3c.dom.HTMLVideoElement org.w3c.dom.HashChangeEvent
          org.w3c.dom.HashChangeEventInit org.w3c.dom.History org.w3c.dom.HitRegionOptions org.w3c.dom.Image
          org.w3c.dom.ImageBitmap org.w3c.dom.ImageBitmapOptions org.w3c.dom.ImageBitmapRenderingContext
          org.w3c.dom.ImageBitmapRenderingContextSettings org.w3c.dom.ImageBitmapSource org.w3c.dom.ImageData
          org.w3c.dom.ImageOrientation org.w3c.dom.ImageSmoothingQuality org.w3c.dom.ItemArrayLike
          org.w3c.dom.Location org.w3c.dom.MediaError org.w3c.dom.MediaProvider org.w3c.dom.MediaQueryList
          org.w3c.dom.MediaQueryListEvent org.w3c.dom.MediaQueryListEventInit org.w3c.dom.MessageChannel
          org.w3c.dom.MessageEvent org.w3c.dom.MessageEventInit org.w3c.dom.MessagePort org.w3c.dom.MimeType
          org.w3c.dom.MimeTypeArray org.w3c.dom.MutationObserver org.w3c.dom.MutationObserverInit
          org.w3c.dom.MutationRecord org.w3c.dom.NamedNodeMap org.w3c.dom.Navigator
          org.w3c.dom.NavigatorConcurrentHardware org.w3c.dom.NavigatorContentUtils
          org.w3c.dom.NavigatorCookies org.w3c.dom.NavigatorID org.w3c.dom.NavigatorLanguage
          org.w3c.dom.NavigatorOnLine org.w3c.dom.NavigatorPlugins org.w3c.dom.Node org.w3c.dom.NodeFilter
          org.w3c.dom.NodeIterator org.w3c.dom.NodeList org.w3c.dom.NonDocumentTypeChildNode
          org.w3c.dom.NonElementParentNode org.w3c.dom.Option org.w3c.dom.PageTransitionEvent
          org.w3c.dom.PageTransitionEventInit org.w3c.dom.ParentNode org.w3c.dom.Path2D org.w3c.dom.Plugin
          org.w3c.dom.PluginArray org.w3c.dom.PopStateEvent org.w3c.dom.PopStateEventInit
          org.w3c.dom.PremultiplyAlpha org.w3c.dom.ProcessingInstruction org.w3c.dom.PromiseRejectionEvent
          org.w3c.dom.PromiseRejectionEventInit org.w3c.dom.RadioNodeList org.w3c.dom.Range
          org.w3c.dom.RelatedEvent org.w3c.dom.RelatedEventInit org.w3c.dom.RenderingContext
          org.w3c.dom.ResizeQuality org.w3c.dom.Screen org.w3c.dom.ScrollBehavior
          org.w3c.dom.ScrollIntoViewOptions org.w3c.dom.ScrollLogicalPosition org.w3c.dom.ScrollOptions
          org.w3c.dom.ScrollRestoration org.w3c.dom.ScrollToOptions org.w3c.dom.SelectionMode
          org.w3c.dom.ShadowRoot org.w3c.dom.ShadowRootInit org.w3c.dom.ShadowRootMode org.w3c.dom.SharedWorker
          org.w3c.dom.SharedWorkerGlobalScope org.w3c.dom.Slotable org.w3c.dom.Storage org.w3c.dom.StorageEvent
          org.w3c.dom.StorageEventInit org.w3c.dom.Text org.w3c.dom.TextMetrics org.w3c.dom.TextTrack
          org.w3c.dom.TextTrackCue org.w3c.dom.TextTrackCueList org.w3c.dom.TextTrackKind
          org.w3c.dom.TextTrackList org.w3c.dom.TextTrackMode org.w3c.dom.TimeRanges org.w3c.dom.Touch
          org.w3c.dom.TouchEvent org.w3c.dom.TouchList org.w3c.dom.TrackEvent org.w3c.dom.TrackEventInit
          org.w3c.dom.TreeWalker org.w3c.dom.UnionAudioTrackOrTextTrackOrVideoTrack
          org.w3c.dom.UnionElementOrHTMLCollection org.w3c.dom.UnionElementOrMouseEvent
          org.w3c.dom.UnionElementOrRadioNodeList org.w3c.dom.UnionHTMLOptGroupElementOrHTMLOptionElement
          org.w3c.dom.UnionMessagePortOrWindow org.w3c.dom.UnionMessagePortOrWindowProxy
          org.w3c.dom.ValidityState org.w3c.dom.VideoTrack org.w3c.dom.VideoTrackList org.w3c.dom.WebSocket
          org.w3c.dom.Window org.w3c.dom.WindowEventHandlers org.w3c.dom.WindowLocalStorage
          org.w3c.dom.WindowOrWorkerGlobalScope org.w3c.dom.WindowSessionStorage org.w3c.dom.Worker
          org.w3c.dom.WorkerGlobalScope org.w3c.dom.WorkerLocation org.w3c.dom.WorkerNavigator
          org.w3c.dom.WorkerOptions org.w3c.dom.WorkerType org.w3c.dom.XMLDocument org.w3c.dom.XMLDocument
          org.w3c.dom.clipboard.Clipboard org.w3c.dom.clipboard.ClipboardEvent
          org.w3c.dom.clipboard.ClipboardEventInit org.w3c.dom.clipboard.ClipboardPermissionDescriptor
          org.w3c.dom.css.CSS org.w3c.dom.css.CSSGroupingRule org.w3c.dom.css.CSSImportRule
          org.w3c.dom.css.CSSMarginRule org.w3c.dom.css.CSSMediaRule org.w3c.dom.css.CSSNamespaceRule
          org.w3c.dom.css.CSSPageRule org.w3c.dom.css.CSSRule org.w3c.dom.css.CSSRuleList
          org.w3c.dom.css.CSSStyleDeclaration org.w3c.dom.css.CSSStyleRule org.w3c.dom.css.CSSStyleSheet
          org.w3c.dom.css.ElementCSSInlineStyle org.w3c.dom.css.LinkStyle org.w3c.dom.css.MediaList
          org.w3c.dom.css.StyleSheet org.w3c.dom.css.StyleSheetList
          org.w3c.dom.css.UnionElementOrProcessingInstruction org.w3c.dom.encryptedmedia.MediaEncryptedEvent
          org.w3c.dom.encryptedmedia.MediaEncryptedEventInit org.w3c.dom.encryptedmedia.MediaKeyMessageEvent
          org.w3c.dom.encryptedmedia.MediaKeyMessageEventInit org.w3c.dom.encryptedmedia.MediaKeyMessageType
          org.w3c.dom.encryptedmedia.MediaKeySession org.w3c.dom.encryptedmedia.MediaKeySessionType
          org.w3c.dom.encryptedmedia.MediaKeyStatus org.w3c.dom.encryptedmedia.MediaKeyStatusMap
          org.w3c.dom.encryptedmedia.MediaKeySystemAccess
          org.w3c.dom.encryptedmedia.MediaKeySystemConfiguration
          org.w3c.dom.encryptedmedia.MediaKeySystemMediaCapability org.w3c.dom.encryptedmedia.MediaKeys
          org.w3c.dom.encryptedmedia.MediaKeysRequirement org.w3c.dom.events.CompositionEvent
          org.w3c.dom.events.CompositionEventInit org.w3c.dom.events.Event org.w3c.dom.events.EventListener
          org.w3c.dom.events.EventModifierInit org.w3c.dom.events.EventTarget org.w3c.dom.events.FocusEvent
          org.w3c.dom.events.FocusEventInit org.w3c.dom.events.InputEvent org.w3c.dom.events.InputEventInit
          org.w3c.dom.events.KeyboardEvent org.w3c.dom.events.KeyboardEventInit org.w3c.dom.events.MouseEvent
          org.w3c.dom.events.MouseEventInit org.w3c.dom.events.UIEvent org.w3c.dom.events.UIEventInit
          org.w3c.dom.events.WheelEvent org.w3c.dom.events.WheelEventInit org.w3c.dom.mediacapture.Capabilities
          org.w3c.dom.mediacapture.ConstrainBooleanParameters
          org.w3c.dom.mediacapture.ConstrainDOMStringParameters org.w3c.dom.mediacapture.ConstrainDoubleRange
          org.w3c.dom.mediacapture.ConstrainULongRange org.w3c.dom.mediacapture.ConstrainablePattern
          org.w3c.dom.mediacapture.ConstraintSet org.w3c.dom.mediacapture.Constraints
          org.w3c.dom.mediacapture.DoubleRange org.w3c.dom.mediacapture.InputDeviceInfo
          org.w3c.dom.mediacapture.MediaDeviceInfo org.w3c.dom.mediacapture.MediaDeviceKind
          org.w3c.dom.mediacapture.MediaDevices org.w3c.dom.mediacapture.MediaStream
          org.w3c.dom.mediacapture.MediaStreamConstraints org.w3c.dom.mediacapture.MediaStreamTrack
          org.w3c.dom.mediacapture.MediaStreamTrackEvent org.w3c.dom.mediacapture.MediaStreamTrackEventInit
          org.w3c.dom.mediacapture.MediaStreamTrackState org.w3c.dom.mediacapture.MediaTrackCapabilities
          org.w3c.dom.mediacapture.MediaTrackConstraintSet org.w3c.dom.mediacapture.MediaTrackConstraints
          org.w3c.dom.mediacapture.MediaTrackSettings org.w3c.dom.mediacapture.MediaTrackSupportedConstraints
          org.w3c.dom.mediacapture.OverconstrainedErrorEvent
          org.w3c.dom.mediacapture.OverconstrainedErrorEventInit org.w3c.dom.mediacapture.Settings
          org.w3c.dom.mediacapture.ULongRange org.w3c.dom.mediacapture.VideoFacingModeEnum
          org.w3c.dom.mediacapture.VideoResizeModeEnum org.w3c.dom.mediasource.AppendMode
          org.w3c.dom.mediasource.EndOfStreamError org.w3c.dom.mediasource.MediaSource
          org.w3c.dom.mediasource.ReadyState org.w3c.dom.mediasource.SourceBuffer
          org.w3c.dom.mediasource.SourceBufferList org.w3c.dom.parsing.DOMParser
          org.w3c.dom.parsing.XMLSerializer org.w3c.dom.parsing.XMLSerializer
          org.w3c.dom.pointerevents.PointerEvent org.w3c.dom.pointerevents.PointerEventInit
          org.w3c.dom.svg.GetSVGDocument org.w3c.dom.svg.SVGAElement org.w3c.dom.svg.SVGAngle
          org.w3c.dom.svg.SVGAnimatedAngle org.w3c.dom.svg.SVGAnimatedBoolean
          org.w3c.dom.svg.SVGAnimatedEnumeration org.w3c.dom.svg.SVGAnimatedInteger
          org.w3c.dom.svg.SVGAnimatedLength org.w3c.dom.svg.SVGAnimatedLengthList
          org.w3c.dom.svg.SVGAnimatedNumber org.w3c.dom.svg.SVGAnimatedNumberList
          org.w3c.dom.svg.SVGAnimatedPoints org.w3c.dom.svg.SVGAnimatedPreserveAspectRatio
          org.w3c.dom.svg.SVGAnimatedRect org.w3c.dom.svg.SVGAnimatedString
          org.w3c.dom.svg.SVGAnimatedTransformList org.w3c.dom.svg.SVGBoundingBoxOptions
          org.w3c.dom.svg.SVGCircleElement org.w3c.dom.svg.SVGCursorElement org.w3c.dom.svg.SVGDefsElement
          org.w3c.dom.svg.SVGDescElement org.w3c.dom.svg.SVGElement org.w3c.dom.svg.SVGElementInstance
          org.w3c.dom.svg.SVGEllipseElement org.w3c.dom.svg.SVGFitToViewBox
          org.w3c.dom.svg.SVGForeignObjectElement org.w3c.dom.svg.SVGGElement
          org.w3c.dom.svg.SVGGeometryElement org.w3c.dom.svg.SVGGradientElement
          org.w3c.dom.svg.SVGGraphicsElement org.w3c.dom.svg.SVGHatchElement
          org.w3c.dom.svg.SVGHatchpathElement org.w3c.dom.svg.SVGImageElement org.w3c.dom.svg.SVGLength
          org.w3c.dom.svg.SVGLengthList org.w3c.dom.svg.SVGLineElement org.w3c.dom.svg.SVGLinearGradientElement
          org.w3c.dom.svg.SVGMarkerElement org.w3c.dom.svg.SVGMeshElement
          org.w3c.dom.svg.SVGMeshGradientElement org.w3c.dom.svg.SVGMeshpatchElement
          org.w3c.dom.svg.SVGMeshrowElement org.w3c.dom.svg.SVGMetadataElement org.w3c.dom.svg.SVGNameList
          org.w3c.dom.svg.SVGNumber org.w3c.dom.svg.SVGNumberList org.w3c.dom.svg.SVGPathElement
          org.w3c.dom.svg.SVGPatternElement org.w3c.dom.svg.SVGPointList org.w3c.dom.svg.SVGPolygonElement
          org.w3c.dom.svg.SVGPolylineElement org.w3c.dom.svg.SVGPreserveAspectRatio
          org.w3c.dom.svg.SVGRadialGradientElement org.w3c.dom.svg.SVGRectElement org.w3c.dom.svg.SVGSVGElement
          org.w3c.dom.svg.SVGScriptElement org.w3c.dom.svg.SVGSolidcolorElement org.w3c.dom.svg.SVGStopElement
          org.w3c.dom.svg.SVGStringList org.w3c.dom.svg.SVGStyleElement org.w3c.dom.svg.SVGSwitchElement
          org.w3c.dom.svg.SVGSymbolElement org.w3c.dom.svg.SVGTSpanElement org.w3c.dom.svg.SVGTests
          org.w3c.dom.svg.SVGTextContentElement org.w3c.dom.svg.SVGTextElement
          org.w3c.dom.svg.SVGTextPathElement org.w3c.dom.svg.SVGTextPositioningElement
          org.w3c.dom.svg.SVGTitleElement org.w3c.dom.svg.SVGTransform org.w3c.dom.svg.SVGTransformList
          org.w3c.dom.svg.SVGURIReference org.w3c.dom.svg.SVGUnitTypes org.w3c.dom.svg.SVGUnknownElement
          org.w3c.dom.svg.SVGUseElement org.w3c.dom.svg.SVGUseElementShadowRoot org.w3c.dom.svg.SVGViewElement
          org.w3c.dom.svg.SVGZoomAndPan org.w3c.dom.svg.ShadowAnimation org.w3c.dom.url.URL
          org.w3c.dom.url.URLSearchParams org.w3c.fetch.Body org.w3c.fetch.Headers org.w3c.fetch.Request
          org.w3c.fetch.RequestCache org.w3c.fetch.RequestCredentials org.w3c.fetch.RequestDestination
          org.w3c.fetch.RequestInit org.w3c.fetch.RequestMode org.w3c.fetch.RequestRedirect
          org.w3c.fetch.RequestType org.w3c.fetch.Response org.w3c.fetch.ResponseInit
          org.w3c.fetch.ResponseType org.w3c.files.Blob org.w3c.files.BlobPropertyBag org.w3c.files.File
          org.w3c.files.FileList org.w3c.files.FilePropertyBag org.w3c.files.FileReader
          org.w3c.files.FileReaderSync org.w3c.notifications.GetNotificationOptions
          org.w3c.notifications.Notification org.w3c.notifications.NotificationAction
          org.w3c.notifications.NotificationDirection org.w3c.notifications.NotificationEvent
          org.w3c.notifications.NotificationEventInit org.w3c.notifications.NotificationOptions
          org.w3c.notifications.NotificationPermission org.w3c.performance.GlobalPerformance
          org.w3c.performance.Performance org.w3c.performance.PerformanceNavigation
          org.w3c.performance.PerformanceTiming org.w3c.workers.Cache org.w3c.workers.CacheBatchOperation
          org.w3c.workers.CacheQueryOptions org.w3c.workers.CacheStorage org.w3c.workers.Client
          org.w3c.workers.ClientQueryOptions org.w3c.workers.ClientType org.w3c.workers.Clients
          org.w3c.workers.ExtendableEvent org.w3c.workers.ExtendableEventInit
          org.w3c.workers.ExtendableMessageEvent org.w3c.workers.ExtendableMessageEventInit
          org.w3c.workers.FetchEvent org.w3c.workers.FetchEventInit org.w3c.workers.ForeignFetchEvent
          org.w3c.workers.ForeignFetchEventInit org.w3c.workers.ForeignFetchOptions
          org.w3c.workers.ForeignFetchResponse org.w3c.workers.FrameType org.w3c.workers.FunctionalEvent
          org.w3c.workers.InstallEvent org.w3c.workers.RegistrationOptions org.w3c.workers.ServiceWorker
          org.w3c.workers.ServiceWorkerContainer org.w3c.workers.ServiceWorkerGlobalScope
          org.w3c.workers.ServiceWorkerMessageEvent org.w3c.workers.ServiceWorkerMessageEventInit
          org.w3c.workers.ServiceWorkerRegistration org.w3c.workers.ServiceWorkerState
          org.w3c.workers.UnionClientOrMessagePortOrServiceWorker
          org.w3c.workers.UnionMessagePortOrServiceWorker org.w3c.workers.WindowClient org.w3c.xhr.FormData
          org.w3c.xhr.ProgressEvent org.w3c.xhr.ProgressEventInit org.w3c.xhr.XMLHttpRequest
          org.w3c.xhr.XMLHttpRequest org.w3c.xhr.XMLHttpRequestEventTarget
          org.w3c.xhr.XMLHttpRequestEventTarget org.w3c.xhr.XMLHttpRequestResponseType
          org.w3c.xhr.XMLHttpRequestResponseType org.w3c.xhr.XMLHttpRequestUpload
          org.w3c.xhr.XMLHttpRequestUpload'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window kotlin_hierarchy_completions "

        print_completers $orgW3c

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 4 OF 5 --------------------------------------------------------------- #
      evaluate-commands %sh{

        kotlinTest='kotlin.test.AfterClass
          kotlin.test.AfterTest
          kotlin.test.Asserter
          kotlin.test.AsserterContributor
          kotlin.test.AssertionResult
          kotlin.test.BeforeClass
          kotlin.test.BeforeTest
          kotlin.test.DefaultAsserter
          kotlin.test.FrameworkAdapter
          kotlin.test.Ignore
          kotlin.test.junit5.JUnit5Asserter
          kotlin.test.junit5.JUnit5Contributor
          kotlin.test.junit.JUnitAsserter
          kotlin.test.junit.JUnitContributor
          kotlin.test.Test
          kotlin.test.testng.TestNGAsserter
          kotlin.test.testng.TestNGContributor'

        # ------------------------------------------------------------------------------------------ #
        print_completers() {

          format=' %s||%s'

          for import in $@
          do
            printf "$format" "$import" "$import"
          done

        }
        # ------------------------------------------------------------------------------------------ #
        printf "set-option -add window kotlin_hierarchy_completions "

        print_completers $kotlinTest

        printf "\n"
        # ------------------------------------------------------------------------------------------ #
      }
      # --------------- STAGE 5 OF 5 --------------------------------------------------------------- #
      # NOTE: Hesitant to implement as a diverse community exists and growing.
      #       Perhaps through a kakoune module would be best.
      # Maybe going TODO: ktor <https://api.ktor.io/>
      #
    } catch %{
      set-option window kotlin_hierarchy_completions
    }
  }
  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window kotlin-import-statements }
}
