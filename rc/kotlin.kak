# References --------------------------------------------------------------------------------------- #
# ‾‾‾‾‾‾‾‾‾‾
# Team: Yerlan & Kirk Duncan
#
# Kotlin 2021, Types, kotlinx-serialization, Core & Json api, viewed 28 July 2021, <https://kotlin.github.io/kotlinx.serialization/kotlinx-serialization-core/kotlinx-serialization-core/index.html>
# Kotlin 2021, README, kotlinx.serialization/formats, viewed 28 July 2021, <https://github.com/Kotlin/kotlinx.serialization/blob/master/formats/README.md>
# Kotlin 2020, Keywords and Operators, v1.4.0, viewed 9 September 2020, https://kotlinlang.org/docs/reference/keyword-reference.html
# Kdoc 2020, Documenting Kotlin Code, Block Tags, v1.4.0, viewed 9 September 2020, https://kotlinlang.org/docs/reference/kotlin-doc.html
# Oracle 2020, Java Platform, Standard Edition & Java Development Kit, Version 14 API Specification, viewed 8 September 2020, https://docs.oracle.com/en/java/javase/14/docs/api/index.html
#
# File types --------------------------------------------------------------------------------------- #
# ‾‾‾‾‾‾‾‾‾‾
hook global BufCreate .*[.](kt|kts)  %{
  set-option buffer filetype kotlin
}

# Initialization ----------------------------------------------------------------------------------- #
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾
hook global WinSetOption filetype=kotlin %{
  require-module kotlin

  set-option window static_words %opt{kotlin_static_words}

  # cleanup trailing whitespaces when exiting insert mode
  hook window ModeChange pop:insert:.* -group kotlin-indents kotlin-trim-indents
  hook window InsertChar \n -group kotlin-inserts kotlin-insert-on-newlines
  hook window InsertChar \n -group kotlin-indents kotlin-indent-on-newlines
  hook window InsertChar \{ -group kotlin-indents kotlin-indent-on-opening-curly-braces
  hook window InsertChar \} -group kotlin-indents kotlin-indent-on-closing-curly-braces

  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window kotlin-.+ }
}

# -override flag is required to prevent 'duplicate id' with rc/filetype/kotlin.kak
hook -group kotlin-highlighter global WinSetOption filetype=kotlin %{
  add-highlighter -override window/kotlin ref kotlin
  add-highlighter -override window/kdoc ref kdoc

  hook -once -always window WinSetOption filetype=.* %{
    remove-highlighter window/kotlin
    remove-highlighter window/kdoc
  }
}

hook global BufSetOption filetype=kotlin %{
  require-module kotlin

  set-option buffer comment_line '//'
  set-option buffer comment_block_begin '/*'
  set-option buffer comment_block_end '*/'

  hook -once -always buffer BufSetOption filetype=.* %{ remove-hooks buffer kotlin-.+ }
}

# Module ------------------------------------------------------------------------------------------- #
# ‾‾‾‾‾‾
provide-module -override kotlin %§

add-highlighter shared/kotlin regions
add-highlighter shared/kotlin/code default-region group
add-highlighter shared/kotlin/character     region %{'} %{'(?=[,\)>\s"`\]])} group
add-highlighter shared/kotlin/rawstring     region %{"""} %{(?<!\\)"""} group
add-highlighter shared/kotlin/escapedstring region %{"(?=")} %{"} group
add-highlighter shared/kotlin/string        region %{"} %{(?<!\\)"} group
add-highlighter shared/kotlin/comment       region /\* \*/ fill comment
add-highlighter shared/kotlin/inline_docs   region /// $ fill documentation
add-highlighter shared/kotlin/line_comment  region // $ fill comment

add-highlighter shared/kotlin/code/annotations regex @\w+\b|\b\w+@(?=\{) 0:meta
add-highlighter shared/kotlin/code/identifiers regex \b(field|it)\b 1:variable
add-highlighter shared/kotlin/code/fields      regex \.([A-Za-z_][\w]*)\s*?\. 1:default

# String and Character mawww/kakoune/yerlaser:#4204 <https://github.com/mawww/kakoune/pull/4204>
add-highlighter shared/kotlin/character/     fill value
add-highlighter shared/kotlin/character/     regex %{'.{1}(.+?)'} 1:value
add-highlighter shared/kotlin/rawstring/     fill string
add-highlighter shared/kotlin/rawstring/     regex (\$\{.+?\}) 1:value
add-highlighter shared/kotlin/escapedstring/ fill string
add-highlighter shared/kotlin/escapedstring/ regex (\$\{.+?\}) 1:value

# prion: <https://discuss.kakoune.com/t/add-highlighter-regex-getting-the-middle-bit-args-drop-1-jointostring/1807/2>
# fail: ${args.joinToString(" ")} due to double " " or an unbalanced "
# match: \" \" or \" not \" " as unbalanced.
add-highlighter shared/kotlin/string/ fill string
add-highlighter shared/kotlin/string/ regex (\$\{.+?\}|\$[0-9A-Z_a-z]+) 1:value

# As at 15 March 2021, kotlin_method see: https://regex101.com/r/Mhy4HG/1
add-highlighter shared/kotlin/code/methods regex ::([A-Za-z_][\w]*)|\.([A-Za-z_][\w]*)\s*?[\(\{]|\.([A-Za-z_][\w]*)[\s\)\}>](?=[^\(\{]) 1:function 2:default 3:default

# Test suite functions: fun `this is a valid character function test`()
add-highlighter shared/kotlin/code/fun_tests  regex ^\h*?fun\s*?`(.[^<>:/\[\]\\\.]+?)`\h*?(?=\() 1:default+iuf
add-highlighter shared/kotlin/code/delimiters regex (\(|\)|\[|\]|\{|\}|\;|') 1:operator
add-highlighter shared/kotlin/code/operators  regex (\+|-|\*|&|=|\\|\?|%|\|-|!|\||->|\.|,|<|>|:|\^|/) 1:operator
add-highlighter shared/kotlin/code/numbers    regex \b((0(x|X)[0-9a-fA-F]*)|(([0-9]+\.?[0-9]*)|(\.[0-9]+))((e|E)(\+|-)?[0-9]+)?)([LlFf])?\b 0:value

# Generics need improvement, as after a colon will match as a constant only.
# val program: IOU = XXXX; val cat: DOG = XXXX. matches IOU or DOG as a
# CONSTANT when it could be generics. See: https://regex101.com/r/VPO5LE/10
add-highlighter shared/kotlin/code/constants_and_generics regex \b((?<==\h)\([A-Z][A-Z0-9_]+(?=[<:\;])|(?<!<)[A-Z][A-Z0-9_]+\b(?!<[>\)]))|\b((?<!=\s)(?<!\.)[A-Z]+\d*?(?![\(\;:])(?=[,\)>\s]))\b 1:meta 2:type

add-highlighter shared/kotlin/code/target regex @(delegate|field|file|get|param|property|receiver|set|setparam)(?=:) 0:meta
add-highlighter shared/kotlin/code/soft   regex \b(?<!\.)(by|catch|constructor|dynamic|finally|get|import|init|set|where)\b 1:keyword
add-highlighter shared/kotlin/code/hard   regex \b(as|as\?|break|class|continue|do|else|false|for|fun|if|in|!in|interface|is|!is|null|object|package|return|super|this|throw|true|try|typealias|val|var|when|while)\b 1:keyword

add-highlighter shared/kotlin/code/modifier regex \b(actual|abstract|annotation|companion|const|crossinline|data|enum|expect|external|final|infix|inline|inner|internal|lateinit|noinline|open|operator|out|override|private|protected|public|reified|sealed|suspend|tailrec|vararg)\b(?=[\s\n]) 1:attribute

add-highlighter shared/kotlin/code/type regex \b(Annotation|Any|Boolean|BooleanArray|Byte|ByteArray|Char|Character|CharArray|CharSequence|Class|ClassLoader|Cloneable|Comparable|Compiler|DeprecationLevel|Double|DoubleArray|Enum|Float|FloatArray|Function|Int|IntArray|Integer|Lazy|LazyThreadSafetyMode|Long|LongArray|Math|Nothing|Number|Object|Package|Pair|Process|Runnable|Runtime|SecurityManager|Short|ShortArray|StackTraceElement|StrictMath|String|StringBuffer|System|Thread|ThreadGroup|ThreadLocal|Triple|Unit|Void)\b(?=[^<]) 1:type

# Kdoc --------------------------------------------------------------------------------------------- #
# ‾‾‾‾
add-highlighter shared/kdoc group
add-highlighter shared/kdoc/tag regex \*(?:\s+)?(@(author|constructor|exception|param|property|receiver|return|sample|see|since|suppress|throws))\b 1:default+ui

# Discolour ---------------------------------------------------------------------------------------- #
# ‾‾‾‾‾‾‾‾‾
add-highlighter shared/kotlin/code/discolour regex ^(package|import)(?S)(.+) 2:default+fa

# Commands source edited c-family.kak: <https://raw.githubusercontent.com/mawww/kakoune/master/rc/filetype/c-family.kak>
# ‾‾‾‾‾‾‾‾
# NOTE: c-family.kak - indent and insert commands: no i didn't, but you can <https://delapouite.github.io/kakoune-explain/>
# NOTE: error running hook InsertChar( )/kotlin-insert: 1:1: 'kotlin-insert-on-new-line': no such command
define-command -override -hidden kotlin-insert-on-new-line %[ ]
define-command -override -hidden kotlin-indent-on-new-line %[ ]
define-command -override -hidden kotlin-indent-on-opening-curly-brace %[ ]
define-command -override -hidden kotlin-indent-on-closing-curly-brace %[ ]

define-command -hidden kotlin-trim-indents %{
  # remove the line if it's empty when leaving the insert mode
  try %{ execute-keys -draft x 1s^(\h+)$<ret> d }
}

define-command -hidden kotlin-indent-on-newlines %~
  evaluate-commands -draft -itersel %<
    try %{
      # if previous line is part of a comment, do nothing
      execute-keys -draft <a-?>/\*<ret> <a-K>^\h*[^/*\h]<ret>
    } catch %{
      # preserve previous line indent
      execute-keys -draft <semicolon>K<a-&>
    }
    # indent after lines ending with { or (
    try %[ execute-keys -draft kx <a-k> [{(]\h*$ <ret> j<a-gt> ]
    # cleanup trailing white spaces on the previous line
    try %{ execute-keys -draft kx s \h+$ <ret>d }
    # align to opening paren of previous line
    try %{ execute-keys -draft [( <a-k> \A\([^\n]+\n[^\n]*\n?\z <ret> s \A\(\h*.|.\z <ret> '<a-;>' & }
    # indent after a pattern match on when/where statements
    try %[ execute-keys -draft kx <a-k> ^\h*(when|where).*$ <ret> j<a-gt> ]
    # indent after term on an expression
    try %[ execute-keys -draft kx <a-k> =\h*?$ <ret> j<a-gt> ]
    # indent after keywords
    try %[ execute-keys -draft <semicolon><a-F>)MB <a-k> \A(catch|do|else|for|if|try|while)\h*\(.*\)\h*\n\h*\n?\z <ret> s \A|.\z <ret> 1<a-&>1<a-space><a-gt> ]
    # indent after term on an expression or a where clause
    # works for EVERY `fun` and `where` keyword, the NEXT continuation will be indented, FIXME: ALL subsequent will NOT.
    try %< execute-keys -draft k x <a-k>^\h*?(?:\b(private|public|protected|internal)\b)?\h*?(?:\b(tailrec|inline|infix|operator|external|suspend)\b)?\h*?\b(fun|where)\b[\S\s]*?$ <ret> j<a-gt>
    > catch %<
        execute-keys -draft JK <a-&>
    >
    # indent after term on an expression
    # try %< execute-keys -draft k<a-x> <a-k> =\h*?$ <ret> j<a-gt> >
    # deindent closing brace(s) when after cursor
    try %[ execute-keys -draft x <a-k> ^\h*[})] <ret> gh / [})] <ret> m <a-S> 1<a-&> ]
  >
~

define-command -hidden kotlin-indent-on-opening-curly-braces %[
  # align indent with opening paren when { is entered on a new line after the closing paren
  try %[ execute-keys -draft -itersel h<a-F>)M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> <a-S> 1<a-&> ]
]

define-command -hidden kotlin-indent-on-closing-curly-braces %[
  # align to opening curly brace when alone on a line
  try %[ execute-keys -itersel -draft <a-h><a-:><a-k>^\h+\}$<ret>hm<a-S>1<a-&> ]
]

define-command -hidden kotlin-insert-on-newlines %[ evaluate-commands -itersel -draft %[
  execute-keys <semicolon>
  try %[
    evaluate-commands -draft -save-regs '/"' %[
      # copy the commenting prefix
      execute-keys -save-regs '' k x1s^\h*(//+\h*)<ret> y
      try %[
        # if the previous comment isn't empty, create a new one
        execute-keys x<a-K>^\h*//+\h*$<ret> jxs^\h*<ret>P
      ] catch %[
        # if there is no text in the previous comment, remove it completely
        execute-keys d
      ]
    ]

    # trim trailing whitespace on the previous line
    try %[ execute-keys -draft k x s\h+$<ret> d ]
  ]
  try %[
    # if the previous line isn't within a comment scope, break
    execute-keys -draft kx <a-k>^(\h*/\*|\h+\*(?!/))<ret>

    # find comment opening, validate it was not closed, and check its using star prefixes
    execute-keys -draft <a-?>/\*<ret><a-H> <a-K>\*/<ret> <a-k>\A\h*/\*([^\n]*\n\h*\*)*[^\n]*\n\h*.\z<ret>

    try %[
      # if the previous line is opening the comment, insert star preceeded by space
      execute-keys -draft kx<a-k>^\h*/\*<ret>
      execute-keys -draft i*<space><esc>
    ] catch %[
      try %[
        # if the next line is a comment line insert a star
        execute-keys -draft jx<a-k>^\h+\*<ret>
        execute-keys -draft i*<space><esc>
      ] catch %[
        try %[
          # if the previous line is an empty comment line, close the comment scope
          execute-keys -draft kx<a-k>^\h+\*\h+$<ret> x1s\*(\h*)<ret>c/<esc>
        ] catch %[
          # if the previous line is a non-empty comment line, add a star
          execute-keys -draft i*<space><esc>
        ]
      ]
    ]

    # trim trailing whitespace on the previous line
    try %[ execute-keys -draft k x s\h+$<ret> d ]
    # align the new star with the previous one
    execute-keys Kx1s^[^*]*(\*)<ret>&
  ]
] ]

# Exceptions, Errors, and Types -------------------------------------------------------------------- #
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
# macro: 93le<a-;>i<ret><esc><esc>
evaluate-commands %sh{

  kotlin_keywords='abstract actual annotation as break by catch class companion const
    constructor continue crossinline data delegate do dynamic else enum expect external
    false field file final finally for fun get if import in infix init inline inner
    interface internal is lateinit noinline null object open operator out override
    package param private property protected public receiver reified return sealed set
    setparam super suspend tailrec this throw true try typealias val var vararg when where while'

  kotlin_types='Annotation Any Boolean BooleanArray Byte ByteArray Char Character CharArray
    CharSequence Class ClassLoader Cloneable Comparable Compiler DeprecationLevel Double
    DoubleArray Enum Float FloatArray Function Int IntArray Integer Lazy LazyThreadSafetyMode
    Long LongArray Math Nothing Number Object Package Pair Process Runnable Runtime
    SecurityManager Short ShortArray StackTraceElement StrictMath String StringBuffer System
    Thread ThreadGroup ThreadLocal Triple Unit Void'

  # ------------------------------------------------------------------------------------------------ #

  kdocs='author constructor exception param property receiver return sample see since suppress throws'

  # ------------------------------------------------------------------------------------------------ #

  kotlin_errors='Error AssertionError NotImplementedError OutOfMemoryError'

  kotlin_exceptions='CharacterCodingException IllegalCallableAccessException
    IllegalPropertyDelegateAccessException NoSuchPropertyException RuntimeException Throwable'

  # ------------------------------------------------------------------------------------------------ #

  java_errors='AbstractMethodError AnnotationFormatError AssertionError AWTError
    BootstrapMethodError ClassCircularityError ClassFormatError CoderMalfunctionError
    ExceptionInInitializerError FactoryConfigurationError GenericSignatureFormatError
    IllegalAccessError IncompatibleClassChangeError InstantiationError InternalError IOError
    JMXServerErrorException​ LinkageError NoClassDefFoundError NoSuchFieldError NoSuchMethodError
    OutOfMemoryError RuntimeErrorException RuntimeErrorException​ SchemaFactoryConfigurationError
    ServerError​ ServiceConfigurationError StackOverflowError ThreadDeath
    TransformerFactoryConfigurationError UnknownError UnsatisfiedLinkError
    UnsupportedClassVersionError VerifyError VirtualMachineError ZipError'

  # ------------------------------------------------------------------------------------------------ #

  java_exceptions='AbsentInformationException AcceptPendingException AccessControlException
    AccessDeniedException AccessException AccountException AccountExpiredException
    AccountLockedException AccountNotFoundException ActivateFailedException ActivationException
    AEADBadTagException AgentInitializationException AgentLoadException AlreadyBoundException
    AlreadyBoundException AlreadyConnectedException AnnotationTypeMismatchException
    ArithmeticException ArrayIndexOutOfBoundsException ArrayStoreException
    AsynchronousCloseException AtomicMoveNotSupportedException AttachNotSupportedException
    AttachOperationFailedException AttributeInUseException AttributeModificationException
    AttributeNotFoundException AuthenticationException AuthenticationException
    AuthenticationNotSupportedException AWTException BackingStoreException
    BadAttributeValueExpException BadBinaryOpValueExpException BadLocationException
    BadPaddingException BadStringOperationException BatchUpdateException BindException
    BrokenBarrierException BufferOverflowException BufferUnderflowException CancellationException
    CancelledKeyException CannotProceedException CannotRedoException CannotUndoException
    CardException CardNotPresentException CatalogException CertificateEncodingException
    CertificateEncodingException CertificateException CertificateException
    CertificateExpiredException CertificateExpiredException CertificateNotYetValidException
    CertificateNotYetValidException CertificateParsingException CertificateParsingException
    CertificateRevokedException CertPathBuilderException CertPathValidatorException
    CertStoreException ChangedCharSetException CharacterCodingException CharConversionException
    ClassCastException ClassInstallException ClassNotFoundException ClassNotLoadedException
    ClassNotPreparedException CloneNotSupportedException ClosedByInterruptException
    ClosedChannelException ClosedConnectionException ClosedDirectoryStreamException
    ClosedFileSystemException ClosedSelectorException ClosedWatchServiceException CMMException
    CommunicationException CompletionException ConcurrentModificationException
    ConfigurationException ConnectException ConnectException ConnectIOException
    ConnectionPendingException ContextNotEmptyException CredentialException
    CredentialExpiredException CredentialNotFoundException CRLException DataFormatException
    DataTruncation DatatypeConfigurationException DateTimeException DateTimeParseException
    DestroyFailedException DigestException DirectoryIteratorException DirectoryNotEmptyException
    DOMException DuplicateFormatFlagsException DuplicateRequestException EmptyStackException
    EngineTerminationException EnumConstantNotPresentException EOFException EvalException
    EventException Exception ExecutionControlException ExecutionException
    ExemptionMechanismException ExpandVetoException ExportException FailedLoginException
    FileAlreadyExistsException FileLockInterruptionException FileNotFoundException FilerException
    FileSystemAlreadyExistsException FileSystemException FileSystemLoopException
    FileSystemNotFoundException FindException FontFormatException
    FormatFlagsConversionMismatchException FormatterClosedException GeneralSecurityException
    GSSException HeadlessException HttpConnectTimeoutException HttpRetryException
    HttpTimeoutException IIOException IIOInvalidTreeException IllegalAccessException
    IllegalArgumentException IllegalBlockingModeException IllegalBlockSizeException
    IllegalCallerException IllegalChannelGroupException IllegalCharsetNameException
    IllegalClassFormatException IllegalComponentStateException IllegalConnectorArgumentsException
    IllegalFormatCodePointException IllegalFormatConversionException IllegalFormatException
    IllegalFormatFlagsException IllegalFormatPrecisionException IllegalFormatWidthException
    IllegalMonitorStateException IllegalPathStateException IllegalReceiveException
    IllegalSelectorException IllegalStateException IllegalThreadStateException
    IllegalUnbindException IllformedLocaleException ImagingOpException
    InaccessibleObjectException IncompatibleThreadStateException IncompleteAnnotationException
    InconsistentDebugInfoException IndexOutOfBoundsException InputMismatchException
    InstanceAlreadyExistsException InstanceNotFoundException InstantiationException
    InsufficientResourcesException InternalException InternalException
    InterruptedByTimeoutException InterruptedException InterruptedIOException
    InterruptedNamingException IntrospectionException IntrospectionException
    InvalidAlgorithmParameterException InvalidApplicationException
    InvalidAttributeIdentifierException InvalidAttributesException InvalidAttributeValueException
    InvalidAttributeValueException InvalidClassException InvalidCodeIndexException
    InvalidDnDOperationException InvalidKeyException InvalidKeyException InvalidKeySpecException
    InvalidLineNumberException InvalidMarkException InvalidMidiDataException
    InvalidModuleDescriptorException InvalidModuleException InvalidNameException
    InvalidObjectException InvalidOpenTypeException InvalidParameterException
    InvalidParameterSpecException InvalidPathException InvalidPreferencesFormatException
    InvalidPropertiesFormatException InvalidRelationIdException InvalidRelationServiceException
    InvalidRelationTypeException InvalidRequestStateException InvalidRoleInfoException
    InvalidRoleValueException InvalidSearchControlsException InvalidSearchFilterException
    InvalidStackFrameException InvalidStreamException InvalidTargetObjectTypeException
    InvalidTypeException InvocationException InvocationTargetException IOException JarException
    JarSignerException JMException JMRuntimeException JMXProviderException
    JMXServerErrorException JSException JShellException KeyAlreadyExistsException KeyException
    KeyManagementException KeySelectorException KeyStoreException LambdaConversionException
    LayerInstantiationException LdapReferralException LimitExceededException
    LineUnavailableException LinkException LinkLoopException ListenerNotFoundException
    LoginException LSException MalformedInputException MalformedLinkException
    MalformedObjectNameException MalformedParameterizedTypeException MalformedParametersException
    MalformedURLException MarshalException MarshalException MBeanException
    MBeanRegistrationException MidiUnavailableException MimeTypeParseException
    MirroredTypeException MirroredTypesException MissingFormatArgumentException
    MissingFormatWidthException MissingResourceException MonitorSettingException
    NameAlreadyBoundException NameNotFoundException NamingException NamingSecurityException
    NativeMethodException NegativeArraySizeException NoConnectionPendingException
    NoInitialContextException NoninvertibleTransformException NonReadableChannelException
    NonWritableChannelException NoPermissionException NoRouteToHostException
    NoSuchAlgorithmException NoSuchAttributeException NoSuchDynamicMethodException
    NoSuchElementException NoSuchFieldException NoSuchFileException NoSuchMechanismException
    NoSuchMethodException NoSuchObjectException NoSuchPaddingException NoSuchProviderException
    NotActiveException NotBoundException NotCompliantMBeanException NotContextException
    NotDirectoryException NotImplementedException NotLinkException NotSerializableException
    NotYetBoundException NotYetConnectedException NullPointerException NumberFormatException
    ObjectCollectedException ObjectStreamException OpenDataException
    OperationNotSupportedException OperationsException OptionalDataException
    OverlappingFileLockException ParseException ParserConfigurationException
    PartialResultException PatternSyntaxException PortUnreachableException PrinterAbortException
    PrinterException PrinterIOException PrintException PrivilegedActionException
    ProfileDataException PropertyVetoException ProtocolException ProviderException
    ProviderMismatchException ProviderNotFoundException RangeException RasterFormatException
    ReadOnlyBufferException ReadOnlyFileSystemException ReadPendingException ReferralException
    ReflectionException ReflectiveOperationException RefreshFailedException
    RejectedExecutionException RelationException RelationNotFoundException
    RelationServiceNotRegisteredException RelationTypeNotFoundException RemoteException
    ResolutionException ResolutionException RMISecurityException RoleInfoNotFoundException
    RoleNotFoundException RowSetWarning RunException RuntimeErrorException RuntimeException
    RuntimeMBeanException RuntimeOperationsException SaslException SAXException
    SAXNotRecognizedException SAXNotSupportedException SAXParseException SchemaViolationException
    ScriptException SecurityException SerialException ServerCloneException ServerError
    ServerException ServerNotActiveException ServerRuntimeException ServiceNotFoundException
    ServiceUnavailableException ShortBufferException ShutdownChannelGroupException
    SignatureException SizeLimitExceededException SkeletonMismatchException
    SkeletonNotFoundException SocketException SocketSecurityException SocketTimeoutException
    SPIResolutionException SQLClientInfoException SQLDataException SQLException
    SQLFeatureNotSupportedException SQLIntegrityConstraintViolationException
    SQLInvalidAuthorizationSpecException SQLNonTransientConnectionException
    SQLNonTransientException SQLRecoverableException SQLSyntaxErrorException SQLTimeoutException
    SQLTransactionRollbackException SQLTransientConnectionException SQLTransientException
    SQLWarning SSLException SSLHandshakeException SSLKeyException SSLPeerUnverifiedException
    SSLProtocolException StoppedException StreamCorruptedException StringConcatException
    StringIndexOutOfBoundsException StubNotFoundException SyncFactoryException
    SyncFailedException SyncProviderException TimeLimitExceededException TimeoutException
    TooManyListenersException TransformerConfigurationException TransformerException
    TransformException TransportTimeoutException TypeNotPresentException UncheckedIOException
    UndeclaredThrowableException UnexpectedException UnknownAnnotationValueException
    UnknownDirectiveException UnknownElementException UnknownEntityException
    UnknownFormatConversionException UnknownFormatFlagsException UnknownGroupException
    UnknownHostException UnknownHostException UnknownObjectException UnknownServiceException
    UnknownTypeException UnmappableCharacterException UnmarshalException
    UnmodifiableClassException UnmodifiableModuleException UnmodifiableSetException
    UnrecoverableEntryException UnrecoverableKeyException UnresolvedAddressException
    UnresolvedReferenceException UnsupportedAddressTypeException UnsupportedAudioFileException
    UnsupportedCallbackException UnsupportedCharsetException UnsupportedEncodingException
    UnsupportedFlavorException UnsupportedLookAndFeelException UnsupportedOperationException
    UnsupportedTemporalTypeException URIReferenceException URISyntaxException UserException
    UserPrincipalNotFoundException UTFDataFormatException VMCannotBeModifiedException
    VMDisconnectedException VMMismatchException VMOutOfMemoryException VMStartException
    WebSocketHandshakeException WriteAbortedException WritePendingException
    WrongMethodTypeException XAException XMLParseException XMLSignatureException
    XMLStreamException XPathException XPathException XPathExpressionException
    XPathFactoryConfigurationException XPathFunctionException ZipException ZoneRulesException'

  # ------------------------------------------------------------------------------------------------ #

  serializationCoreJson='BinaryFormat Contextual ContextualSerializer DeserializationStrategy
    ExperimentalSerializationApi InternalSerializationApi KSerializer Polymorphic
    PolymorphicSerializer Required SealedClassSerializer SerialFormat SerialInfo Serializable
    SerializationException SerializationStrategy Serializer SerialName StringFormat Transient
    UseContextualSerialization UseSerializers Json JsonArray JsonArrayBuilder JsonBuilder
    JsonConfiguration JsonContentPolymorphicSerializer JsonDecoder JsonElement JsonEncoder
    JsonNames JsonNull JsonObject JsonObjectBuilder JsonPrimitive JsonTransformingSerializer
    DescriptorData LongAsStringSerializer ClassSerialDescriptorBuilder PolymorphicKind
    PrimitiveKind SerialDescriptor SerialKind StructureKind AbstractDecoder AbstractEncoder
    CompositeDecoder CompositeEncoder Decoder Encoder PolymorphicModuleBuilder
    PolymorphicProvider SerializersModule SerializersModuleBuilder SerializersModuleCollector'

  # ------------------------------------------------------------------------------------------------ #
  # c-family.kak: <https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak#L271>
  join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
  # ---------------------------------------------------------------------------------------------- #
  add_highlighter() { printf "add-highlighter shared/kotlin/code/ regex %s %s\n" "$1" "$2"; }
  # ---------------------------------------------------------------------------------------------- #
  # alexherbo2: <https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108>
  add_word_highlighter() {

    while [ $# -gt 0 ]; do
      words=$1 face=$2; shift 2
      regex="\\b($(join "${words}" '|'))\\b"
      add_highlighter "$regex" "1:$face"
    done

  }
  # ---------------------------------------------------------------------------------------------- #
  # lesson learnt: style macro in for loop use: $@ not "$@"
  print_static_words() {

    for name in $@
    do
      printf " %s" "$name"
    done

  }
  # ---------------------------------------------------------------------------------------------- #
  printf "declare-option str-list kotlin_static_words "

  print_static_words $kotlin_keywords $kotlin_types $kdocs
  print_static_words $kotlin_errors $kotlin_exceptions
  print_static_words $java_exceptions $java_errors
  print_static_words $serializationCoreJson

  printf "\n"
  # ------------------------------------------------------------------------------------------------ #
  add_word_highlighter "$kotlin_errors" "rgb:d26937" "$kotlin_exceptions" "rgb:d26937"
  add_word_highlighter "$java_errors" "rgb:d26937" "$java_exceptions" "rgb:d26937"

  printf "\n"
  # ------------------------------------------------------------------------------------------------ #
}
§
# ------------------------------------------------------------------------------------------------- #
