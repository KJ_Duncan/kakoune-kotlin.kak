/**
 * Returns the line, paragraph, sentence, and word count from $kak_selection.
 * Regular expressions: feel the inner Neanderthal.
 *
 * kotlinc -o countWords countWords.kt -opt
 *
 * map global user 'w'-docstring \
 * "count line, paragraph, sentence, and words from selection" \
 * %{: echo %sh{printf "%s" "${kak_selection}" | <path-to>/countWords.kexe }<ret>}
 *
 * kotlinc countWords.kt -d countWords.jar
 *
 * map global user 'w'-docstring \
 * "count line, paragraph, sentence, and words from selection" \
 * %{: echo %sh{printf "%s" "${kak_selection}" | java -jar <path-to>/countWords.jar }<ret>}
 */
fun String.countLines(): Int {
    return Regex("\r\n|\r|\n")
        .findAll(this)
        .count() + 1
}

fun String.countParagraphs(): Int {
    return Regex("[^\r\n]+((\r|\n|\r\n)[^\r\n]+)*")
        .findAll(this)
        .count()
}

fun String.countSentences(): Int {
    return Regex("(?<=[\'\"\"A-Za-z0-9][.!?])\\s+(?=[A-Z])")
        .findAll(this)
        .count()
}

fun String.countWords(): Int {
    return Regex("\\b[^\\d\\W]+\\b")
        .findAll(this)
        .count()
}

fun main() {
    val stdin = generateSequence(::readLine)
    val words = stdin.joinToString("\n")
    println("Kotlin Vajazzle: "
        + "L:=${words.countLines()} "
        + "P:=${words.countParagraphs()} "
        + "S:=${words.countSentences()} "
        + "W:=${words.countWords()}")
}
