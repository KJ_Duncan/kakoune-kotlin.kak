### [Kotlin](https://kotlinlang.org/docs/reference/)


See countWords.kt for an initial usage script and view the [asciicast to see countWords.kt in action](https://asciinema.org/a/245929)  

[![](/res/kakoune-kotlin-kt-asciicast.png)](https://asciinema.org/a/245929)  

Freeloading goodies conveniently available at [Kotlin is Awesome!](https://kotlin.link/)  

---

#### [Install](https://kotlinlang.org/docs/tutorials/command-line.html#downloading-the-compiler)

```bash
$ curl -s "https://get.sdkman.io" | bash  
$ source "$HOME/.sdkman/bin/sdkman-init.sh"  
$ sdk install java  
$ sdk install kotlin  
$ sdk install kscript  
```

---

#### Usage


##### [Native](https://kotlinlang.org/docs/tutorials/native/basic-kotlin-native-app.html#compiling-and-examining-output)


[kotlinc-native](https://github.com/JetBrains/kotlin-native/releases/) download binary distrbution `https://github.com/JetBrains/kotlin/releases/tag/<version>`  
`export PATH=kotlin-native-<platform>-<version>/bin:$PATH`  
  
```bash
$ kotlinc -o <name> <name>.kt -opt  
```

---

##### [JVM](https://kotlinlang.org/docs/tutorials/command-line.html#creating-and-running-a-first-application)

```bash
$ kotlinc <name>.kt -d <name>.jar  
$ java -jar <name>.jar  
$ javajar.sh <name>.jar  
```  
  
```bash
javajar.sh
#!/usr/bin/env bash
exec java -jar "$1" "${@:2}"
exit 1
```

---

##### [JS](https://kotlinlang.org/docs/tutorials/javascript/getting-started-command-line/command-line-library-js.html#creating-a-kotlinjavascript-library)

```bash
$ kotlinc-js -output <name>.js -meta-info <name>.kt  
```

---

##### [Kscript](https://github.com/holgerbrandl/kscript)


kscript header  
`#!/usr/bin/env kscript`  

```bash
$ ./<name>.kts  
$ kscript <name>.kts  
$ kotlinc -script <name>.kts  
```

[kscript as a substitute for awk](http://holgerbrandl.github.io/kotlin/2017/05/08/kscript_as_awk_substitute.html)  

---

##### [Android](https://kotlinlang.org/docs/reference/android-overview.html)  


And if you like bouncing a ball against a wall check out [Kotlin for Android](https://kotlinlang.org/docs/reference/android-overview.html)  

---

Kotlin: pretty _f'n_ sweet... cue a theme [song spotify](https://open.spotify.com/track/4bz7uB4edifWKJXSDxwHcs?si=o0ap8C8rShiUUm0O_Pe_eQ) || [song youtube](https://www.youtube.com/watch?v=Xu3FTEmN-eg)  

---


`<iframe src="https://pl.kotl.in/Wjyxk_-TT?theme=darcula&readOnly=true"></iframe>`
  
[![](/res/Kotlin_Playground_Edit-Run-Share_Kotlin-Code-Online.png)](https://pl.kotl.in/Wjyxk_-TT?theme=darcula&readOnly=true)


