/**
 * Returns the line, paragraph, sentence, and word count from $kak_selection.
 * Regular expressions: feel the inner Neanderthal.
 *
 * map global user 'w'-docstring \
 * "count line, paragraph, sentence, and words from selection" \
 * %{: echo %sh{printf "%s" "${kak_selection}" | kotlinc -script <path-to>/countWords.kts }<ret>}
 *
 * map global user 'w'-docstring \
 * "count line, paragraph, sentence, and words from selection" \
 * %{: echo %sh{printf "%s" "${kak_selection}" | kscript <path-to>/countWords.kts }<ret>}
 */
fun String.countLines(): Int {
    return Regex("\r\n|\r|\n")
        .findAll(this)
        .count() + 1
}

fun String.countParagraphs(): Int {
    return Regex("[^\r\n]+((\r|\n|\r\n)[^\r\n]+)*")
        .findAll(this)
        .count()
}

fun String.countSentences(): Int {
    return Regex("(?<=[\'\"\"A-Za-z0-9][.!?])\\s+(?=[A-Z])")
        .findAll(this)
        .count()
}

fun String.countWords(): Int {
    return Regex("\\b[^\\d\\W]+\\b")
        .findAll(this)
        .count()
}

val stdin = generateSequence(::readLine)
val words = stdin.joinToString("\n")
println("Kotlin Vajazzle: "
    + "L:=${words.countLines()} "
    + "P:=${words.countParagraphs()} "
    + "S:=${words.countSentences()} "
    + "W:=${words.countWords()}")
