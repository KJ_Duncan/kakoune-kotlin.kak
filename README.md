### kakoune-kotlin


[Kotlin](https://kotlinlang.org/) support for the [Kakoune](https://kakoune.org/) editor.  
  
See kt/countWords.(kt|kts) for an initial usage script and view the [asciicast to see countWords.kt in action](https://asciinema.org/a/245929)  

![](/res/kak-kotlin-countWords.png)


----


Supported file extensions: `(kt|kts)`  


----


#### Install


```text
# github/mawww/yerlaser/kotlin.kak#4167.

provide-module -override kotlin
---------------^^^^^^^^^
```

_'allow the module to replace an existing one with the same name.'_  
@see [Module commands](https://github.com/mawww/kakoune/blob/master/doc/pages/commands.asciidoc#module-commands)  
  
For the original [kakoune/rc/filetype/kotlin.kak](https://github.com/mawww/kakoune/blob/master/rc/filetype/kotlin.kak)  

```shell
# Built from and runs on kakoune commit:
# commit 021da117cf90bf25b65e3344fa8e43ab4262b714
# Date:   Sat Aug 13 18:53:27 2022 +0100

# kakoune build environment
export CXX="g++-12"
```


----


```shell
$ mv rc/*.kak $HOME/.config/kak/autoload/
# or
$ mv rc/*.kak $XDG_CONFIG_HOME/kak/autoload/
```

[andreyorst-plug.kak](https://github.com/andreyorst/plug.kak)  
```
plug "KJ_Duncan/kakoune-kotlin.kak" domain "bitbucket.org"
```


----


#### mawww/kakoune


-  yerlaser [kotlin-patches](https://github.com/mawww/kakoune/commits/master/rc/filetype/kotlin.kak)
-  Github issue Merged [Add Kotlin filetype #4167](https://github.com/mawww/kakoune/pull/4167) as at 29 April 2021 is now supported.
-  Github issue [Add Kotlin filetype #4112](https://github.com/mawww/kakoune/pull/4112)
-  Github issue [Add Kotlin filetype #4073](https://github.com/mawww/kakoune/pull/4073)
-  yerlaser on [discuss kakoune](https://discuss.kakoune.com/t/kakoune-kotlin-kak-a-plain-kak-filetype/1260/4)


Building the community, well done yerlaser.  


----


#### Out-Of-The-Box


Lenormf's kakoune welcome mat for users.  
```
# Allow cycling to the next/previous candidate with <tab> and <s-tab> when completing a word

hook global InsertCompletionShow .* %{
  try %{
    execute-keys -draft 'h<a-K>\h<ret>'
    map window insert <tab> <c-n>
    map window insert <s-tab> <c-p>
  }
}

hook global InsertCompletionHide .* %{
  unmap window insert <tab> <c-n>
  unmap window insert <s-tab> <c-p>
}

# if trailing whitespaces is an issue do
# and not where whitespaces matter e.g. markdown
# <https://discuss.kakoune.com/t/kakoune-hook-negative-selector/1867/6> 
hook global BufWritePre '.*\.(kt|kts)$' %{ try %{ execute-keys -draft '%s\h+$<ret>d' } }
```

-  Lenormand, F 2018, out-of-the-box, oob.kak, viewed 28 February 2021, https://github.com/lenormf/out-of-the-box/blob/master/oob.kak#L6
-  Lenormand, F 2020, kakoune-extra, viewed 01 March 2021, https://github.com/lenormf/kakoune-extra


----


#### References


-  CodeMirror 2020, clike.js, The in-browser code editor, viewed 9 September 2020, https://github.com/codemirror/CodeMirror/blob/master/mode/clike/clike.js#L650
-  Kakoune 2021, c-family.kak, viewed 29 January 2021, https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak
-  Kdoc 2020, Documenting Kotlin Code, Block Tags, v1.4.0, viewed 9 September 2020, https://kotlinlang.org/docs/reference/kotlin-doc.html
-  Kostyukov, V 2018, Kotlin.JSON-tmLanguage, Kotlin Sublime Text Package, viewed 9 September 2020, https://github.com/vkostyukov/kotlin-sublime-package/blob/master/Kotlin.JSON-tmLanguage
-  Kotlin 2020, Keywords and Operators, v1.4.0, viewed 9 September 2020, https://kotlinlang.org/docs/reference/keyword-reference.html
-  Ktor 2020, Ktor API documentation, v1.4.0, viewed 9 September 2020, https://api.ktor.io/
-  Oracle 2020, Java® Platform, Standard Edition & Java Development Kit, Version 14 API Specification, viewed 8 September 2020, https://docs.oracle.com/en/java/javase/14/docs/api/index.html


----


#####  TODO


- [ ] regex for generics and constants needs improvement https://regex101.com/r/VPO5LE/7
- [ ] indentation on keywords in edge cases
- [ ] additional syntax rules [TornadoFX](https://tornadofx.io/) / [kotlinx](https://github.com/Kotlin/)
- [x] error running hook InsertChar( )/kotlin-insert: 1:1: 'kotlin-insert-on-new-line': no such command, duplicates the `////` comment line if shares same name with `rc/filetype/kotlin.kak`.
- [x] [snippets collection](https://github.com/andreyorst/kakoune-snippet-collection) pull request
- [x] [kdoc](https://kotlinlang.org/docs/reference/kotlin-doc.html) syntax support
- [x] [ktor](https://ktor.io/) _partial_ syntax support


@see snippet libraries; [occivink](https://github.com/occivink/kakoune-snippets) and [andreyorst](https://github.com/andreyorst/kakoune-snippet-collection), or [alexherbo2](https://github.com/alexherbo2/snippets.kak).  

  
Known indentation issues, on the todo list  
```java
do {
    val y = retrieveData()
} while (y != null)
    // bad indentation on statement conclusion
```

```java
fun <T> copyWhenGreater(list: List<T>, threshold: T): List<String>
  where T : CharSequence,
    T : Comparable<T> {
      return list.filter { it > threshold }.map { it.toString() }
  }
```

[correct indentation format](https://kotlinlang.org/docs/reference/generics.html#generic-constraints)
```java
fun <T> copyWhenGreater(list: List<T>, threshold: T): List<String>
    where T : CharSequence,
          T : Comparable<T> {
    return list.filter { it > threshold }.map { it.toString() }
}
```


----


#### Kotlin Information


Kotlin is a multiplatform programming language for Native / JVM / JS / Android:  

* [Kotlin Native](https://kotlinlang.org/docs/reference/native-overview.html)
* [Kotlin Server-side](https://kotlinlang.org/docs/reference/server-overview.html)
* [Kotlin JS](https://kotlinlang.org/docs/reference/js-overview.html)
* [Kotlin Android](https://kotlinlang.org/docs/reference/android-overview.html)
* [Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
* [Multiplatform Kotlin library](https://kotlinlang.org/docs/tutorials/multiplatform-library.html)
* [Multiplatform Programming](https://kotlinlang.org/docs/reference/multiplatform.html)
* [Create React Kotlin App](https://github.com/JetBrains/create-react-kotlin-app)
* [Kotlin Standard Library](https://kotlinlang.org/api/latest/jvm/stdlib/index.html)
* [Arrow Functional Kotlin](https://arrow-kt.io/)


----


For a quick start into Kotlin review these Github repositories:  

* [Design Patterns in Kotlin](https://github.com/dbacinski/Design-Patterns-In-Kotlin)
* [Kotlin Algorithm Club](https://github.com/bmaslakov/kotlin-algorithm-club)
* [Kotlin Data Science Resources](https://github.com/thomasnield/kotlin-data-science-resources)


----


Modification of the original kakoune file: [java.kak](https://github.com/mawww/kakoune/blob/master/rc/filetype/java.kak).  
  
Opinionated:  
  
Kotlin [coding-conventions](https://kotlinlang.org/docs/reference/coding-conventions.html)  
[ktlint](https://github.com/pinterest/ktlint#--) is a kotlin linter with a builtin formatter [ktlint/Main.kt](https://github.com/pinterest/ktlint/blob/master/ktlint/src/main/kotlin/com/pinterest/ktlint/Main.kt#L55).  
`set-option window lintcmd 'ktlint'`  
  
Kakoune default colour [codes](https://github.com/mawww/kakoune/blob/master/colors/default.kak) and [faces](https://github.com/mawww/kakoune/blob/master/doc/pages/faces.asciidoc):  

* value: red
* type,operator: yellow
* variable,module,attribute: green
* function,string,comment: cyan
* keyword: blue
* meta: magenta
* builtin: default


----


##### Kakoune General Information


Work done? Have some fun. Share any improvements, ideas or thoughts with the community [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 210+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  


----


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.  
